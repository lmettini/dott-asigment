# Black and white bitmap

## Solution
Given the specified input and assuming that it'll have the correct format, the script will iterate over the bitmap looking for white dots, for each white dot using BFS we will keep track of the distance from any other coordinate of the bitmap to that white point, if there is an already shorter known distance, that mean that we won't continue the BFS on that coordinate for that white spot

## Commands
* `npm run compile`: compile typescript script
* `npm run start` run the script using the standart stdin
* `npm run start:testfile`: run the script using the content of the file test/testInput as stdin
* `npm run eslint`: runs eslint validaion, eslint needs to be preinstaled

## Step to test it
* `npm install`
* `npm run compile`
* `npm run start:testfile`
And you should see in the output the result of the 2 test cases in the file test/testInput