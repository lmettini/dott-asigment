export class DistancesMap {
    private distances : Number[][];
    
    constructor( n : Number, m : Number) {
        this.distances = Array(n).fill(Number.NaN).map(() => Array(m).fill(-1));
    }

    getN(){
        return this.distances.length;
    }

    getM(){
        return this.distances[0].length;
    }

    setCloserPointDistance(x : number, y : number, proposedDistacence : number){
        // we check first if it's closer
        if(this.distances[x][y] ==  -1 || proposedDistacence<this.distances[x][y]){
            this.distances[x][y] = proposedDistacence;

            return true;
        } else {
            return false;
        }
    }

    prettyPrintRow(row : number){
        return this.distances[row].join(' ');
    }
}