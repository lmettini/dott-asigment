import { DistancesMap } from './distancesMap';

process.stdin.resume();
process.stdin.setEncoding('utf-8');

let inputString: string = '';
let inputLines : Array<String>;

process.stdin.on('data', inputStdin => {
    inputString += inputStdin;
});

process.stdin.on('end', () => {
    inputLines = String(inputString).replace(/\s*$/, '')
        .split('\n')
        .map(str => str.replace(/\s*$/, ''));

    main();
});

function readLine() {
    return inputLines.shift() || '';
}

function main() {

    let testCases : Number = Number(readLine());
    
    for(let c=0; c<testCases; c++){

        // we get the coordinates for this case
        let coordinates : Array<String> = readLine().split(' ');
        let n : Number = Number(coordinates[0]);
        let m : Number = Number(coordinates[1]);

        let bitmap : Array<Array<String>> = Array();

        // we populate the bitmap from the current case
        for(var i : number = 0; i<n; i++){
            bitmap[i] = readLine().split('');
        }

        let distances : DistancesMap = new DistancesMap(n,m);
    
        // calculate distances, we apply bfs everytime we find a white spot
        for(var i : number = 0; i<n; i++){
            for(var j : number = 0; j<m; j++){
                if(bitmap[i][j] == '1'){
                    calculateDistance(distances, i, j, 0);
                }
            }
        }

        // print results
        for(var i : number = 0; i<n; i++){
            console.log(distances.prettyPrintRow(i));
        }
    }    
}

function calculateDistance( distances : DistancesMap,
                            x : number,
                            y : number,
                            actualDistance : number) {
    let n = distances.getN();
    let m = distances.getM();

    if(x>=0 && x<n && y>=0 && y<m && distances.setCloserPointDistance(x,y,actualDistance)){
        // if we are closer, we need to continue
        calculateDistance(distances, x-1, y,actualDistance+1);
        calculateDistance(distances, x, y-1,actualDistance+1);
        calculateDistance(distances, x+1, y,actualDistance+1);
        calculateDistance(distances, x, y+1,actualDistance+1);
    }
}